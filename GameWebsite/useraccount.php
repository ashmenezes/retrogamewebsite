<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The Retro Gamer - Home</title>
    
    <link rel="apple-touch-icon" sizes="57x57" href="icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="icons/favicon-16x16.png">
    <link rel="manifest" href="icons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="js/bootstrap.js">
    <link rel="stylesheet" href="style.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
  </head>
<!-- NAVBAR
================================================== -->    
    <body>
  <div class="container">
    <div class="jumbotron">
      <a href="retrogamerhome.php" id="retrogamerlogo"></a>
      <!---->
      <div class="navbar-text pull-right" style="inline-block">
      <div class="socialNetwork" style="display:inline;">
        <img src="images/FacebookLike.jpg" class="facebook img-responsive img-rounded" alt="Facebook button" style="width:75px;height:25px;display:inline;">
        <img src="images/TwitterFollow.jpg" class="twitter img-responsive img-rounded" alt="Twitter button" style="width:75px;height:25px;display:inline;">
        <img src="images/YoutubeSubscribe.jpg" class="youtube img-responsive img-rounded" alt="Youtube button" style="width:100px;height:25px;display:inline;">
      </div>
      <!---->
    </div>
    </div>
      <div class="container accountDetails">
        <div class="row">
          <div class="col-md-1 col-md-offset-6">
              
          </div>
        </div>
      </div>
  </div>

<!-- ~~~~~~~~~~~~~~~~~~Navigation Bar ~~~~~~~~~~~~~~~-->

  <<nav class="navbar navbar-default navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="retrogamerhome.php">Home<span class="sr-only">(current)</span></a></li>
        <li><a href="retrogamerAbout.php">About</a></li>
        <li class="dropdown">
          <a if="drop1" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Genre<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
            <li><a href="#">All</a></li>
            <li class="divider"></li>
            <li><a href="#">Action</a></li>
            <li><a href="#">Adventure</a></li>
            <li><a href="#">Arcade</a></li>
            <li><a href="#">Fighting</a></li>
            <li><a href="#">FPS</a></li>
            <li><a href="#">Horror</a></li>
            <li><a href="#">Indie</a></li>
            <li><a href="#">Multiplayer</a></li>
            <li><a href="#">Platformer</a></li>
            <li><a href="#">Puzzle</a></li>
            <li><a href="#">RPG</a></li>
          </ul>
        </li>
        <li><a href="gamelist.php">Store</a></li>
      </ul>

      <form class="navbar-form navbar-left" role="search" action="SearchGame.php" method="get">
        <div class="form-group">
          <input type="text" name="g" class="form-control" placeholder="Search Games">
        </div>
        <button type="submit" class="btn btn-danger">Go!</button>
      </form>
      
      <ul class="nav navbar-nav navbar-right">
        <li class="log">
          <?php
            if(isset($_SESSION['myusername'])){
              $myusername=$_SESSION['myusername'];
              echo "<a href=\"useraccount.php\">".$myusername."</a>";
              //echo "<button href=\"useraccount.php\" class=\"btn btn-danger\">".$myusername."</button>";
            }
          ?>
        </li>
        <li class="log">
            <?php
              if(!isset($_SESSION['myusername'])){
                echo "<a href=\"main_login.php\">Log In</a>";
              }
            ?>
        </li>
        <li class="log">
            <?php
              if(isset($_SESSION['myusername'])){
                echo "<a href=\"logout.php\">Log Out</a>";
              }
            ?>
        </li>
        <li>
            <?php
              if(!isset($_SESSION['myusername'])){
                echo "<a href=\"retrogamersignup.php\">Sign up</a>";
              }
            ?>
        <li><a href="#"></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<center><?php
echo "<br><br><div class=\"container\">";
echo "<div class=\"gamelisttitle\">";
echo "<img align=\"middle\" src=\"images/Profile.jpg\" alt=\"\">";
echo "</div><br><br></div>";

echo "<div class=\"container gamelist\">";
echo "<p>";

mysql_connect("localhost", "root", "root")or die("cannot connect");
mysql_select_db("theretrogamer")or die("cannot select DB");

$username=$_SESSION['myusername'];

$sql="SELECT username,email,Balance,LoyaltyWallet FROM User WHERE username='$username'";
$result=mysql_query($sql) or die($sql."<br/><br/>".mysql_error());
while ($row = mysql_fetch_array($result)){
	echo $row['username']."<br>";
	echo $row['email']."<br>";
	echo "Online Wallet: £".$row['Balance']."<br>";
}

echo "<br>";
$sql="SELECT UserID,LoyaltyWallet FROM User WHERE username='$username'";
$result=mysql_query($sql) or die($sql."<br/><br/>".mysql_error());
$row=mysql_fetch_array($result);
$uid=$row['UserID'];

$sql2="SELECT count(GameID) FROM Userswithgames WHERE UserID='$uid'";
$result2=mysql_query($sql2)or die($sql2."<br/><br/>".mysql_error());
$row2=mysql_fetch_array($result2);
echo "You have ".$row2[0]." games.<br>";

$sql3="SELECT Game FROM Userswithgames,Gamelist WHERE UserID='$uid' AND Userswithgames.GameID=Gamelist.GCode";
$result3=mysql_query($sql3)or die($sql3."<br/><br/>".mysql_error());
while ($row3=mysql_fetch_array($result3)){
  echo $row3['Game']."<br>";
}
echo "<br>";

$var=$row['LoyaltyWallet']*0.2;

echo "You have " . $row['LoyaltyWallet'] . " Points.<br>";
echo "Exchange rates are now:<br>1 Point = 2 pence.<br>Redeem your points?<br>Exchanging now will give you ".$var." pounds.";
echo "<form class=\"Form\" action=\"http://localhost/gamewebsite/convert.php\" method=\"POST\">";
echo "<br><button type=\"submit\" name=\"convert\" value=".$row['LoyaltyWallet']." class=\"btn-lg btn-success\">Exchange NOW!</button><br><br>";
echo "</form>";
echo "</p>";
echo "</div>";

?></center>
<div class "navbar navbar-default navbar-fixed-bottom">
      <div class="footer">
        <br><br><br><br><br><br>
        <div class= "navbar-text pull-left">Copyright © 2015 The Retro Gamer Inc. <br>All rights reserved. You can also order from The Retro Gamer Store by calling 800-RETRO-0000.</div>
      </div>
  </div>
</body>
</html>