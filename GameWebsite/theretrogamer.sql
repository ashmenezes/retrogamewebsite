DROP TABLE User;
DROP TABLE GameList;
DROP TABLE UsersWithGames;
DROP TABLE Transactions;
DROP TABLE GameDescription;

CREATE TABLE User (
UserID INT PRIMARY KEY AUTO_INCREMENT,
username VARCHAR(25) NOT NULL,
password VARCHAR(25) NOT NULL,
email VARCHAR(25) NOT NULL,
Balance DECIMAL(9,2) NOT NULL,
LoyaltyWallet DECIMAL(9,2)
)ENGINE=INNODB;

CREATE TABLE GameList (
GCode INT PRIMARY KEY AUTO_INCREMENT,
Game VARCHAR(100) NOT NULL,
Genre VARCHAR(25) NOT NULL,
Console VARCHAR(25) NOT NULL,
Price DECIMAL (9,2) NOT NULL,
LoyaltyPts INT (3) NOT NULL
)ENGINE=INNODB;

CREATE TABLE UsersWithGames (
UserID INT,
GameID INT,
FOREIGN KEY (UserID) REFERENCES User(UserID),
FOREIGN KEY (GameID) REFERENCES GameList(GCode)
)ENGINE=INNODB;

CREATE TABLE Trans (
ReceiptID INT PRIMARY KEY AUTO_INCREMENT,
CardNumber INT(16) NOT NULL,
CVV2 INT(3) NOT NULL,
CardName VARCHAR(25) NOT NULL,
ExpiryMonth INT(2) NOT NULL,
ExpiryYear INT(4) NOT NULL,
PurchaseDate DATE NOT NULL,
UserID INT,
FOREIGN KEY (UserID) REFERENCES User(UserID)
)ENGINE=INNODB;

Create TABLE GameTrans(
ReceiptID INT,
GID INT,
FOREIGN KEY (ReceiptID) REFERENCES Trans(ReceiptID),
FOREIGN KEY (GID) REFERENCES GameList(GCode)
)ENGINE=INNODB;

ALTER TABLE `gamelist` ADD `img` VARCHAR(50) NOT NULL AFTER `GCode`;
ALTER TABLE `gamelist` ADD `iwidth` INT NOT NULL ;
ALTER TABLE `gamelist` ADD `iheight` INT NOT NULL ;

INSERT INTO User VALUES ('','Prerna','pb9','pb9@hw.ac.uk',50.0,10);
INSERT INTO User VALUES ('','Ash','am939','am939@hw.ac.uk',50.0,10);
INSERT INTO User VALUES ('','Batyr','bs5','bs5@hw.ac.uk',50.0,10);
INSERT INTO User VALUES ('','Cass','caf1','caf1@hw.ac.uk',50.0,10);
INSERT INTO User VALUES ('','Bello','bs2','bs2@hw.ac.uk',50.0,10);
INSERT INTO User VALUES ('','Misbah','ms788','ms788@hw.ac.uk',50.0,10);
INSERT INTO User VALUES ('','Akash','aml2','aml2@hw.ac.uk',50.0,10);
INSERT INTO User VALUES ('','Waqas','wms31','wms31@hw.ac.uk',50.0,10);
INSERT INTO User VALUES ('','Abdullah','aom31','aom31@hw.ac.uk',50.0,10);
INSERT INTO User VALUES ('','Vishnu','vgk1','vgk1@hw.ac.uk',50.0,10);
INSERT INTO User VALUES ('','Ahmed','aha52','aha52@hw.ac.uk',50.0,10);
INSERT INTO User VALUES ('','Talal','T.A.G.Shaikh','T.A.G.Shaikh@hw.ac.uk',50.0,10);

INSERT INTO GameList VALUES('','Doom','FPS','Atari Jaguar',10.0,5);
INSERT INTO GameList VALUES('','Chrono Trigger','RPG','SNES',20.0,10);
INSERT INTO GameList VALUES('','Pokemon Emerald','RPG','SNES',15.0,5);
INSERT INTO GameList VALUES('','Shin Megami Tensei','RPG','SNES',10.0,5);
INSERT INTO GameList VALUES('','Final Fantasy VI','RPG','SNES',10.0,5);
INSERT INTO GameList VALUES('','Super Mario World','Platformer','SNES',10.0,5);
INSERT INTO GameList VALUES('','Megaman X','Platformer','SNES',20.0,10);
INSERT INTO GameList VALUES('','Legend of Zelda: Link to the Past','RPG','SNES',20.0,10);
INSERT INTO GameList VALUES('','Metal Gear Solid','Action','PlayStation',20.0,10);
INSERT INTO GameList VALUES('','Revelations: Persona','RPG','PlayStation',20.0,10);
INSERT INTO GameList VALUES('','Street Fighter II: The World Warrior','Fighting','SNES',20.0,10);

INSERT INTO UsersWithGames VALUES ('1','1');
INSERT INTO UsersWithGames VALUES ('2','2');
INSERT INTO UsersWithGames VALUES ('3','3');
INSERT INTO UsersWithGames VALUES ('4','4');
INSERT INTO UsersWithGames VALUES ('5','5');
INSERT INTO UsersWithGames VALUES ('6','6');
INSERT INTO UsersWithGames VALUES ('7','7');
INSERT INTO UsersWithGames VALUES ('8','8');
INSERT INTO UsersWithGames VALUES ('1','9');
INSERT INTO UsersWithGames VALUES ('2','10');
INSERT INTO UsersWithGames VALUES ('3','1');
INSERT INTO UsersWithGames VALUES ('4','2');

INSERT INTO Trans VALUES('',1234567891234567,123,'Prerna',12,2020,'2015-03-24',1);

INSERT INTO GameTrans VALUES ('1','1');