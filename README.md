# RetroGameWebsite README #

This README documents whatever steps are necessary to get the application up and running.

### What is this repository for? ###

* Quick summary
This is my first website created for one of my assignments in my 1st year BSc Computer Systems. It's a design for an e-commerce website that sells retro videogames. Please ignore the naming used in this project, it was made a long time ago ;).

### How do I get set up? ###

* Summary of set up
- Download and install XAMPP. Version used for testing is 5.6.32.
- Open the Xampp Control Panel and hit 'Start' for Apache. Two numbers should appear under 'Port(s)', take note of the first one. This is XAMPP's port number.
- Put all the PHP files in the following path:
	<XAMPP installation directory>/xampp/htdocs/<folder name of choice>
- Open your browser and type the following in your address bar:
	localhost:80/<folder name of choice>/retrogamerhome.php
  This should take you to the Home Page.
- Feel free to navigate around the website!

* Database configuration
This website comes with a database file 'theretrogamer.sql'. However, I haven't tested finished testing the file to see if it works, more on this in future versions of the repository.

* Extra Notes
Do remember, this is my FIRST EVER website, so a lot of the scripting methods used are probably not correct. This is being displayed as part of my portfolio purely for the sake of design, and I'm quite proud of it! ;)